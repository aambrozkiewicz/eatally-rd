<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="stylesheet" href="{{ asset('css/app.css') }}">

        <title>kociołek</title>
        <script>window.Laravel = window.Laravel || { csrfToken: '{{ csrf_token() }}' }</script>
    </head>
    <body>
        <div id="app" class="container" style="margin-top: 50px;">
            <div class="row">
                <div class="col-md-3">
                    <div class="list-group">
                        <router-link to="/" class="list-group-item">
                            Menu
                            <div class="pull-right">
                                <span class="glyphicon glyphicon-home"></span>
                            </div>
                        </router-link>
                        <router-link to="/about" class="list-group-item">
                            Jak zamawiać?
                            <div class="pull-right">
                                <span class="glyphicon glyphicon-flash"></span>
                            </div>
                        </router-link>
                    </div>
                </div>
                <div class="col-md-9">
                    <transition name="fade">
                        <router-view></router-view>
                    </transition>
                </div>
            </div>
        </div>

        <script src="{{ asset('js/app.js') }}"></script>
    </body>
</html>
