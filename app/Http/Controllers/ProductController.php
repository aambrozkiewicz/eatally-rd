<?php

namespace App\Http\Controllers;

use App\Product;

class ProductController
{
    function index()
    {
        return response()->json(Product::with('category')->get());
    }
}
