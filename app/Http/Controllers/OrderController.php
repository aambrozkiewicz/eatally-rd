<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|min:6'
        ]);

        return response()->json([
            'name' => $request->get('name'),
            'products' => collect($request->get('products'))
                ->map(function($qty, $productId) {
                    $product = Product::find($productId);

                    return [
                        'name' => $product->name,
                        'qty' => $qty,
                        'total' => $product->price * $qty
                    ];
                })->values()
        ]);
    }
}
